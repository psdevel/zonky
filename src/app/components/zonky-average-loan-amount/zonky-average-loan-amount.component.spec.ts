import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';

import { ZonkyAverageLoanAmountModule } from './zonky-average-loan-amount.module';
import { ZonkyAverageLoanAmountComponent } from './zonky-average-loan-amount.component';
import { ZonkyAverageLoanAmountService } from './zonky-average-loan-amount.service';

describe('ZonkyAverageLoanAmountComponent', () => {
  let service: ZonkyAverageLoanAmountService;
  let component: ZonkyAverageLoanAmountComponent;
  let fixture: ComponentFixture<ZonkyAverageLoanAmountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        ZonkyAverageLoanAmountModule,
      ],
      declarations: [],
      providers: []
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ZonkyAverageLoanAmountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('#Should create the Zonky-average-loan-amount Component', () => {
    expect(component).toBeTruthy();
  });

  it('#Test service, call Zonky API (async)', async () => {
    service = TestBed.get(ZonkyAverageLoanAmountService);
    const response = await service.getLoansAmountByRatingFromMarketplace('X');
    expect(response.status).toEqual(200);
  });
});
