import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { zonkyAverageLoanAmountRoute } from './zonky-average-loan-amount.route';
import { ZonkyAverageLoanAmountComponent } from './zonky-average-loan-amount.component';

import { ZonkyAverageLoanAmountService } from './zonky-average-loan-amount.service';
import { MatButtonModule, MatProgressBarModule, MatSelectModule, MatSnackBarModule } from '@angular/material';
import { CommonModule } from '@angular/common';

@NgModule({
  imports: [
    RouterModule.forChild([zonkyAverageLoanAmountRoute]),
    FormsModule,
    CommonModule,
    MatSelectModule,
    MatButtonModule,
    MatProgressBarModule,
    MatSnackBarModule
  ],
  declarations: [
    ZonkyAverageLoanAmountComponent,
  ],
  providers: [
    ZonkyAverageLoanAmountService,
  ],
  exports: [
    ZonkyAverageLoanAmountComponent,
  ]
})
export class ZonkyAverageLoanAmountModule {}
