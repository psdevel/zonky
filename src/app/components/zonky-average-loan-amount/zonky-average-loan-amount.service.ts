import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Injectable()
export class ZonkyAverageLoanAmountService {
  private readonly ZONKY_API_URL = environment.zonkyApiUrl;
  constructor(private http: HttpClient) {}

  getLoansAmountByRatingFromMarketplace(rating: string, fields: string[] = ['amount'], page?: string, size?: string): Promise<any> {
    const headers: any = [];

    headers['X-Order'] = 'datePublished';
    if (page) { headers['X-Page'] = page; }
    if (size) { headers['X-Size'] = size; }

    return this.http.get( this.ZONKY_API_URL + 'loans/marketplace', {
      observe: 'response',
      params: new HttpParams()
      .set('rating__eq', rating)
      .set('fields', fields.join(',')),
      headers: new HttpHeaders( headers )
    }).toPromise();
  }
}
