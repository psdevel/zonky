import { Component, OnInit } from '@angular/core';
import { ZonkyAverageLoanAmountService } from './zonky-average-loan-amount.service';
import { trigger, style, animate, transition } from '@angular/animations';
import { MatSnackBar } from '@angular/material';
import { Ratings } from './ratings.enum';
import { $enum } from 'ts-enum-util';

@Component({
  selector: 'app-zonky-average-loan-amount',
  templateUrl: './zonky-average-loan-amount.component.html',
  styleUrls: [],
  animations: [
    trigger('textTransition', [
      transition(':enter', [
        style({opacity: 0}),
        animate('500ms ease', style({opacity: 1}))
      ]),
      transition(':leave', [
        animate('500ms ease', style({opacity: 0}))
      ]),
    ])
  ]
})
export class ZonkyAverageLoanAmountComponent implements OnInit {
  readonly responseSize: number = 1000;
  public rating: string;
  public fields: string[] = ['amount', 'datePublished', 'rating'];
  public ratings: {value: string, viewValue: string}[] = [];

  public totalRecords: number;
  public currentProgress: number;

  public amountSum: number = 0;
  public averageLoanAmount: number;

  constructor(
    private service: ZonkyAverageLoanAmountService,
    private snackBar: MatSnackBar
  ) { }

  ngOnInit() {
    $enum(Ratings).forEach((description, rating) => {
      this.ratings.push({value: rating, viewValue: description});
    });
  }

  public callLoanAverageSum() {
    if (!this.averageLoanAmount) {
      this.iterateLoans()
      .then(() => {
        this.averageLoanAmount = this.amountSum / this.totalRecords;
        this.cleanUp();
      })
      .catch((error) => {
        this.snackBar.open('Rating nenalezen.', 'Ok', {
          duration: 8000
        });
      });
    }
  }

  private iterateLoans(): Promise<any> {
    const promises: Promise<any>[] = [];

    return this.service.getLoansAmountByRatingFromMarketplace(this.rating, this.fields, '0', '1')
    .then(resInit => {
      this.totalRecords = parseInt(resInit.headers.get('X-Total'), 10);
      const pagesMax = Math.ceil(this.totalRecords / this.responseSize) - 1;
      let currentPage = 0;
      let processedPages = 0;

      if (resInit.body.length < 1) {
        this.snackBar.open('Nenalezeny žádné záznamy', 'Ok', {
          duration: 8000
        });
      }

      while (currentPage <= pagesMax) {
        promises.push(this.service.getLoansAmountByRatingFromMarketplace(
          this.rating,
          this.fields,
          currentPage.toString(),
          this.responseSize.toString()
        )
        .then(result => {
          result.body.forEach(loan => {
            this.amountSum += loan.amount;
          });
          processedPages++;
          this.currentProgress = (processedPages / pagesMax) * 100;
        }));
        currentPage++;
      }
      return Promise.all(promises);
    });
  }

  public cleanUp(cleanAverageLoanAmount: boolean = false) {
    this.currentProgress = undefined;
    this.totalRecords = undefined;
    this.amountSum = 0;
    if (cleanAverageLoanAmount) {this.averageLoanAmount = undefined; }
  }
}
