import { Route } from '@angular/router';

import { ZonkyAverageLoanAmountComponent } from './zonky-average-loan-amount.component';

export const zonkyAverageLoanAmountRoute: Route = {
  path: '',
  component: ZonkyAverageLoanAmountComponent,
  data: {
    authorities: [],
  }
};
