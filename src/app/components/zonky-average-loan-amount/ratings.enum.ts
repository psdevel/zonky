export enum Ratings {
  AAAAAA  = 'A***',
  AAAAA   = 'A**',
  AAAA    = 'A*',
  AAA     = 'A++',
  AAE     = 'AA+',
  AA      = 'A+',
  AE      = 'AA',
  A       = 'A',
  B       = 'B',
  C       = 'C',
  D       = 'D'
}
