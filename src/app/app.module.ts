import { registerLocaleData } from '@angular/common';
import localeCs from '@angular/common/locales/cs';
import localeCsExtra from '@angular/common/locales/extra/cs';

import { HttpClientModule } from '@angular/common/http';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ZonkyAverageLoanAmountModule } from 'src/app/components/zonky-average-loan-amount/zonky-average-loan-amount.module';

registerLocaleData(localeCs, 'cs-CZ', localeCsExtra);

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    ZonkyAverageLoanAmountModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
