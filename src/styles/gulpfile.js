// //////////////////////////////////
// REQUIRES
// //////////////////////////////////
const
    gulp = require('gulp'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify_js = require('gulp-uglify'),
    uglify_css = require('gulp-clean-css'),
    sass = require('gulp-sass'),
    gulp_inject = require('gulp-inject'),
    browser_sync = require('browser-sync'),
    merge = require('merge-stream');

// //////////////////////////////////
// VARIABLES
// //////////////////////////////////
const
    appFolder = './',
    sourceFolder = 'src/',
    destinationFolder = 'dist/',
    finalCssName = 'styles';

// //////////////////////////////////
// ENVIRONMENT VARIABLES
// //////////////////////////////////
let
    cssDestinationPath = destinationFolder,
    projectAssetsCss = '../';

// //////////////////////////////////
// TASKS
// //////////////////////////////////
gulp.task('process-scss', function () {
    return gulp.src(sourceFolder + 'scss/style.scss')
        .pipe(sass())
        .pipe(rename(finalCssName + '.css'))
        .pipe(gulp.dest(cssDestinationPath))
        .pipe(uglify_css({compatibility: 'ie8'}))
        .pipe(rename(finalCssName + '.min.css'))
        .pipe(gulp.dest(cssDestinationPath));
});
gulp.task('copy-final-files', function () {
    let css = gulp.src(destinationFolder + '*.min.css')
        .pipe(gulp.dest(projectAssetsCss));
    console.log('Copying files into ' + projectAssetsCss);
    return css;
});

// //////////////////////////////////
// COMPUND TASKS
// //////////////////////////////////
gulp.task('process-files', gulp.series(
    ['process-scss']
));
gulp.task('export', gulp.series(
    ['process-files', 'copy-final-files']
));

// //////////////////////////////////
// WATCH TASKS
// //////////////////////////////////
gulp.task('watch', function () {
    gulp.watch(sourceFolder + 'scss/**/*.scss', gulp.series(['process-scss', 'copy-final-files']));
    browser_sync.init({
        server: {baseDir: appFolder}
    });
});
gulp.task('ng-watch', function () {
    gulp.watch(sourceFolder + 'scss/**/*.scss', gulp.series(['process-scss', 'copy-final-files']));
});

// //////////////////////////////////
// DEFAULT TASKS
// //////////////////////////////////
gulp.task('default', gulp.series(
    ['export']
));

gulp.task('help', function(done) {
    console.log('gulp nebo gulp default         : Zapne se skupina tasků které jsou jako default. V našem defaultu je export.');
    console.log('gulp watch                     : Gulp sleduje změny v .scss, .js a po uložení automaticky přebuildí. Navíc otevře index.html na portu:3000.');
    console.log('gulp process-scss              : Přebuildí scss a vytvoří minifikovaný soubor v cílové složce.');
    console.log('gulp export                    : Zprocessuje scss, které uloží do projektu: ' + projectAssetsCss + ', jako minifikované soubory.');
    console.log('gulp ng-watch                  : Sleduje změny js nebo scss a přebuildí je, vytvoří jejich minifikované soubory a uloží do projektové složky: ' + cssDestinationPath);
    done();
});

