import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getContentPresents() {
    return element(by.css('app-root')).all(by.css('*')).then(webElements => {
      return webElements.length;
    });
  }
}
