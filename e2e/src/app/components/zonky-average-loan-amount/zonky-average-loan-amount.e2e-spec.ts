import { AppPage } from './zonky-average-loan-amount.po';
import { browser, by, logging, protractor } from 'protractor';

describe('Zonky component e2e workspace', () => {
  let page: AppPage;
  let ratingSelect;
  let calculateButton;

  beforeAll(() => {
    page = new AppPage();
    ratingSelect = page.getRatingSelect();
    calculateButton = page.getCalculateButton();
  });

  it('#Calculate button disabled before rating inputed', () => {
    page.navigateTo();
    expect(calculateButton.isEnabled()).toBe(false);
  });

  it('#Calculation progress bar apperars', () => {
    page.ratingSelectOption(0);
    calculateButton.click();
    const progressBar = page.getProgressBar();
    expect(progressBar).toBeDefined();
  });

  it('#Waiting text appears', () => {
    const waitingText = page.getWaitingText();
    expect(waitingText).toBeDefined();
  });

  it('#Result is present', () => {
    const result = page.getResult();
    expect(result).toBeDefined();
    expect(result.getText()).toContain('Kč');
  });

  it('#Re-input should clean data', () => {
    page.ratingSelectOption(1);
    expect(page.getCountOfResultElements()).toBe(0);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
