import { browser, by, element } from 'protractor';

export class AppPage {
  navigateTo() {
    return browser.get(browser.baseUrl) as Promise<any>;
  }

  getRatingSelect() {
    return element(by.id('ratingSelect'));
  }

  ratingSelectOption(option: number) {
    this.getRatingSelect().click();
    return element.all(by.tagName('mat-option')).get(option).click();
  }

  getCalculateButton() {
    return element(by.id('calculate'));
  }

  getProgressBar() {
    return element(by.id('progressbarDataGathering'));
  }

  getWaitingText() {
    return element(by.id('waitingText'));
  }

  getResult() {
    return element(by.id('result'));
  }

  getCountOfResultElements() {
    return element.all(by.id('result')).count();
  }


}
