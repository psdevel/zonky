import { AppPage } from './app.po';
import { browser, logging } from 'protractor';

describe('App e2e workspace', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('#App should have content in <app-root>', () => {
    page.navigateTo();
    expect(page.getContentPresents()).toBeGreaterThan(0);
  });

  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
